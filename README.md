# jQuery Plugin : Autoform #
### The Idea is Simple: Automatic Ajaxified form generator ###
#### APACHE V2.0 : http://www.apache.org/licenses/LICENSE-2.0.html ####

Planned Usage : 

```
#!html

<article id="AutoFormGenerator" data-action=" Some Form Action " >
  <section class="AFChild" data-method="PATCH" data-fieldname="SomeName"></section>
  <section class="AFChild" data-method="PATCH" data-fieldname="SomeOtherName"></section>
  <section class="AFChild" data-method="PATCH" data-fieldname="NotAnotherName"></section>
</article>
```

A Simple way of Init

```
#!javascript

$('#AutoFormGenerator').autoform(); // Using Defaults

```

Custom Styles/Class names 


```
#!javascript

$config = {};
$config.SubmitClass = "btn btn-danger";

$templates = {};
$templates.wrapper = "<section class='SomeCustomClass'>";

$('#AutoFormGenerator').autoform($config,$templates);


```
## NOT STABLE ENOUGH FOR USE ##
### Pull requests/Feature Requests Welcome. ###


The Versioning is as Follows

X.y.z

Where 

* X = Release - ( Stable )
* y = Beta - (Considered stable)
* z = Alpha - Unstable.

So 3.1.1 is a Alpha Release of a beta of version 4.0.0
 