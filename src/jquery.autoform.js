(function ($) {

    "use strict";
    var Object, Obj_Children, Forms, default_config, default_templates, init, config, templates;

    default_config = {
        "childClass": "editable-cell",
        "editButtonClass": "btn btn-link",
        "editButtonContent": "<i class='glyphicon glyphicon-pencil'></i>",
        "formClass": "form-inline",
        "inputClass": "form-control",
        "inputType": "text",
        "inputId": null,
        "SubmitClass": "btn btn-primary",
        "SubmitContent": "<i class='glyphicon glyphicon-check'></i>",
        "CancelClass": "btn btn-danger",
        "CancelContent": "<i class='glyphicon glyphicon-remove'></i>",
        "WrapperClass": "form-group",
        "action": Object.data("action"),
        "type": "POST",
        "debug": false
    };
  
    default_templates = {
        "wrapper": $('<div>').attr({
            style: "display:none ",
            class: config.WrapperClass
        }),
        "form": $('<form>').attr({
            action: config.cfg.action,
            method: config.cfg.type,
            class: config.formClass
        }),
        "input": $('<input>').attr({ 
            type: config.inputType,
            class: config.inputClass,
            id: config.inputId
        }),
        "Submit": $('<input>').attr({ 
            type: "submit",
            class: config.SubmitClass
        }).text(config.SubmitContent),
        "Cancel": $('<input>').attr({
            type: "button", 
            class: config.CancelClass
        }).text(config.CancelContent),
        "Edit": $('<button>').attr({ 
            type: "button", 
            class: config.editButtonClass
        }).text(config.editButtonContent),
        "success": $('<p>').attr({
            class: "alert alert-success"
        }),
        "failure": $('<p>').attr({
            class: "alert alert-danger"
        })
    };

    function debug(obj) {
        if (window.console && window.console.log ) {
           window.console.log( "[-- Active Templates --]" );
           $.each( obj.config, function( name, option ) {
           window.console.log( name+" : "+option );
           });
           window.console.log( "[-- Default Templates --]" );
           $.each( obj.config, function( name, option ) {
           window.console.log( name+" : "+option );
           });
           window.console.log( "[-- Active Configuration --]" );
           $.each( obj.config, function( name, option ) {
           window.console.log( name+" : "+option );
           });
           window.console.log( "[-- Default Configuration --]" );
           $.each( obj.config, function( name, option ) {
           window.console.log( name+" : "+option );
           });
           window.console.log( "[-- Found Editables --]" );
           window.console.log(obj.editables);
        }
    }

var submit     = new Event("autoform.submit");
var cancel     = new Event("autoform.cancel");
var toggle     = new Event("autoform.toggle");
var init       = new Event("autoform.init");
var construct  = new Event("autoform.construct");
var destroy    = new Event("autoform.destroy");  


/* Jquery Hook. and Default Code */
$.fn.autoform = function( user_config , user_templates ) {
    config    = $.extend({}, default_config , user_config );
  	 templates = $.extend({}, default_templates, user_templates );
  	 Object    = this;
  	 Obj_Children = Object.find(config.childClass);
  	 this.autoform.init();
    this.autoform.construct();
    //Need to Register the Events for Submit and Cancel and Toggle
}

/* Helper Functions */
$.fn.autoform.init = function() {
  if(config.debug) {
    debug(Object);
  }
  Forms = [];
  Obj_children.each(function(Child) {
    Forms.push( $(templates.form).append(templates.input, templates.Submit , templates.Cancel).appendTo($(templates.wrapper)));
  });
};
  
$.fn.autoform.construct = function() {
  //Does Actual Page Building.
  var place=0;
  $.each( Obj_Children,function(Element) {
  		  Element.append(Forms[place]);
    	  place++;	
  });
  
};
  
$.fn.autoform.submit = $.ajax({
    url: config.action,
    method: config.method,
    data: $(this).serialize()
});
  
$.fn.autoform.submit.success = function( json_data ) {
    Object.autoform.toggle();
  	 if( json_data.message ) {
      return Object.append(templates.success.text( json_data.message ));
    }	
  	 Object.append(templates.success.text( config.defaultSuccessMessage )); 	
}
  
$.fn.autoform.submit.fail 	= function ( Fail_Event ) {
  if( Fail_Event.ResponseJson.message ) {
       Object.append(templates.failure.text( Fail_Event.ResponseJson.message )); 	
  }
   Object.append(templates.failure.text( config.defaultFailMessage )); 	
}
  
$.fn.autoform.cancel = function() {
  //Handles Cancel
};
  
$.fn.autoform.toggle = function() {
  //Handles the Toggling of the hidden (Or Not!) form.
};
  
$.fn.autoform.destroy = function() {
  // Handles the Destruction of the on Page Elements
  // For Advanced Ajax-y Pages
};

  
  
}( jQuery ));

